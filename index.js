const TelegramBot = require('node-telegram-bot-api')

const db = require('bootstrap-sql')([
  `CREATE TABLE mensajes_reenviados(
    chat_id INTEGER NOT NULL,
    msj_original_id INTEGER NOT NULL,
    msj_reenviado_id INTEGER NOT NULL
  );`,
  `ALTER TABLE mensajes_reenviados ADD COLUMN chat_reenviado_id INTEGER;`,
])

const TOKEN = process.env.TELEGRAM_TOKEN
const CHAT_ID = process.env.CHAT_ID

if (!TOKEN)
  throw 'falta un TELEGRAM_TOKEN. pista: lo tenés que conseguir de @BotFather'
if (!CHAT_ID) throw 'falta una CHAT_ID.'

const bot = new TelegramBot(TOKEN, { polling: true })

async function reenviarMensaje({
  chatId,
  msg,
  anonimo = true,
  replyToId,
  extraOffset,
}) {
  const options = {
    ...(replyToId ? { reply_to_message_id: replyToId } : {}),
    parse_mode: 'Markdown',
  }

  const usuaria = `[${msg.from.first_name}](tg://user?id=${msg.from.id})`

  let fwd
  if (msg.text) {
    const msgText = escapeMessage(msg.text, msg.entities, extraOffset)
    fwd = await bot.sendMessage(
      chatId,
      anonimo ? msgText : `${usuaria} dice: \n\n${msgText}`,
      options,
    )
  } else if (msg.photo || msg.voice || msg.document) {
    const caption = anonimo
      ? msgText
      : `de ${usuaria}${
          msg.caption
            ? `: \n\n${escapeMessage(
                msg.caption,
                msg.caption_entities,
                extraOffset,
              )}`
            : ''
        }`

    if (msg.photo) {
      fwd = await bot.sendPhoto(chatId, msg.photo[msg.photo.length - 1].file_id, {
        ...options,
        caption,
      })
    } else if (msg.voice) {
      fwd = await bot.sendVoice(chatId, msg.voice.file_id, {
        ...options,
        caption,
      })
    } else if (msg.document) {
      fwd = await bot.sendDocument(chatId, msg.document.file_id, {
        ...options,
        caption,
      }, { caption: msg.document.file_name })
    }
  } else {
    fwd = await bot.sendMessage(
      chatId,
      `${anonimo ? 'Se' : usuaria} mandó un mensaje sin soporte por el bot.${
        anonimo ? '' : `\n\n\`\`\`${JSON.stringify(msg, 0, 2, 2)}\`\`\``
      }`,
      options,
    )
  }
  db.prepare(
    `INSERT INTO mensajes_reenviados (
      chat_id, msj_original_id, chat_reenviado_id, msj_reenviado_id
    ) VALUES (?, ?, ?, ?);`,
  ).run(msg.chat.id, msg.message_id, chatId, fwd.message_id)
}

const format = (txt, before, after, entity, extraOffset) => {
  const offset = entity.offset + extraOffset
  return (
    txt.slice(0, offset) +
    before +
    txt.slice(offset, offset + entity.length) +
    after +
    txt.slice(offset + entity.length)
  )
}

function escapeMessage(msgText, entities, extraOffset = 0) {
  msgText = msgText
    .replace(/_/g, '\\_')
    .replace(/\*/g, '\\*')
    .replace(/\[/g, '\\[')
    .replace(/`/g, '\\`')
  let goneWrong = []
  if (entities) {
    let lastText = msgText
    for (const entity of entities) {
      const KNOWN_ENTITIES = [
        'bold',
        'italic',
        'code',
        'url',
        'hashtag',
        'text_link',
        'bot_command',
        'email',
      ]
      if (KNOWN_ENTITIES.indexOf(entity.type) === -1) {
        goneWrong.push(entity)
        continue
      }
      extraOffset += msgText.length - lastText.length
      lastText = msgText
      switch (entity.type) {
        case 'bold':
          msgText = format(msgText, '*', '*', entity, extraOffset)
          break
        case 'italic':
          msgText = format(msgText, '_', '_', entity, extraOffset)
          break
        case 'code':
          msgText = format(msgText, '`', '`', entity, extraOffset)
          break
        case 'text_link':
          msgText = format(
            msgText,
            '[',
            '](' + entity.url + ')',
            entity,
            extraOffset,
          )
          break
      }
    }
  }
  if (goneWrong.length > 0) {
    msgText += `\n\n_*Algo salió mal con el bot, no se pudo parsear ciertas cosas._ \`${JSON.stringify(
      goneWrong,
    )}\``
  }
  return msgText
}

bot.getMe().then(({ username }) => {
  console.log('Escuchando...')
  bot.on('message', async (msg) => {
    const chatId = msg.chat.id
    console.log(msg)
    if (msg.chat.id === msg.from.id) {
      let replyToId
      if (msg.reply_to_message) {
        const item = db
          .prepare(
            'SELECT * FROM mensajes_reenviados WHERE msj_reenviado_id = ? AND chat_id = ?;',
          )
          .get(msg.reply_to_message.message_id, CHAT_ID)
        replyToId = item.msj_original_id
      }
      await reenviarMensaje({
        chatId: CHAT_ID,
        msg,
        anonimo: false,
        replyToId,
      })
    } else if (msg.chat.id == CHAT_ID) {
      if (msg.text) {
        // Comandos
        if (msg.text.startsWith('/anunciar')) {
          const [cmd, ...txts] = msg.text.split(' ')
          const txt = txts.join(' ')
          const items = db
            .prepare('SELECT * FROM mensajes_reenviados WHERE NOT chat_id = ?;')
            .all(CHAT_ID)
            .map(({ chat_id }) => chat_id)
            .filter(
              (item, index, self) =>
                item != CHAT_ID && self.indexOf(item) === index,
            )
          msg.text = txt
          await bot.sendMessage(chatId, 'Enviando mensajes...', {
            reply_to_message_id: msg.message_id,
          })
          const promises = items.map(chat_id =>
            reenviarMensaje({
              chatId: chat_id,
              msg,
              anonimo: true,
              extraOffset: -cmd.length - 1,
            })
          )
          const results = await Promise.allSettled(promises)
          await bot.sendMessage(
            chatId,
            `Los mensajes fueron enviados. ${results.filter(r => r.status === 'rejected').length}/${items.length} fallaron.`,
            { reply_to_message_id: msg.message_id },
          )
          return
        } else if (msg.text.startsWith('/ls')) {
          const cmd = msg.text.split(' ')
          const num = parseInt(cmd[1]) || 25

          const items = db
            .prepare(
              'SELECT * FROM mensajes_reenviados WHERE chat_id = ? ORDER BY rowid DESC LIMIT ?;',
            )
            .all(chatId, num)

          await bot.sendMessage(
            chatId,
            `Los últimos ${items.length} mensajes enviados desde acá:\n\n${
              items.reduce((str, msg) => str + `Original: ${msg.msj_original_id}, reenviado: ${msg.msj_reenviado_id}\n`, '')
            }`,
            { reply_to_message_id: msg.message_id },
          )
          return
        } else if (msg.text.startsWith('/borrar')) {
          const cmd = msg.text.split(' ')
          const id = parseInt(cmd[1]) || msg.reply_to_message && msg.reply_to_message.message_id

          if (id) {
            const item = db
              .prepare(
                'SELECT * FROM mensajes_reenviados WHERE msj_original_id = ? AND chat_id = ?;',
              )
              .get(id, CHAT_ID)
            if (item && item.chat_reenviado_id) {
              try {
                await bot.deleteMessage(item.chat_reenviado_id, item.msj_reenviado_id)
                await bot.sendMessage(
                  chatId,
                  'Ese mensaje fue eliminado.',
                  { reply_to_message_id: item.msj_original_id },
                )
              } catch (error) {
                await bot.sendMessage(
                  chatId,
                  'Por alguna razón no pude eliminar este mensaje.',
                  { reply_to_message_id: msg.message_id },
                )
              }
            } else await bot.sendMessage(
              chatId,
              'No encontré ese mensaje, puede que sea muy viejo.',
              { reply_to_message_id: msg.message_id },
            )
          } else await bot.sendMessage(
            chatId,
            'No entendí esa ID.',
            { reply_to_message_id: msg.message_id },
          )
          return
        }
      }
      if (msg.reply_to_message) {
        const item = db
          .prepare(
            'SELECT * FROM mensajes_reenviados WHERE msj_reenviado_id = ? AND chat_reenviado_id = ?;',
          )
          .get(msg.reply_to_message.message_id, CHAT_ID)
        if (item) await reenviarMensaje({
          chatId: item.chat_id,
          msg,
          anonimo: true,
          replyToId: item.msj_original_id,
        })
        else await bot.sendMessage(
          chatId,
          'No puedo contestar a ese mensaje, no lo tengo en la base de datos.',
          { reply_to_message_id: msg.message_id },
        )
      }
    }
  })
})
